import express from 'express';
import messageQueue from './app/services/MQ/index';
import config from './config';


import expressConfig from './config/express';

const port = process.env.PORT || 3072;
const app = express();

messageQueue(config);
expressConfig(app);

app.listen(port);
logger.info(`Notification Service started on port ${port}`);

export default app;

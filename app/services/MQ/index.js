import newUserConsumer from './consumers/new.user.consumer';


const newUserQueue = (config) => {
    newUserConsumer(config.messageQueue.userRegistration);
};


const messageQueues = (config) => {
    newUserQueue(config);
};

export default messageQueues;

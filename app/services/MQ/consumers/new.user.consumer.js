import amqp from 'amqplib/callback_api';
import config from '../../../../config';
import sendRegistrationEmail from '../../../controllers/registration/send.registration.email';


const newUserConsumer = (queue) => {
    amqp.connect(config.RABBIT_MQ_URL, (err, conn) => {
        if (err) {
            process.exit(400);
        }
        conn.createChannel((err, ch) => {
            ch.assertQueue(queue, { durable: true });
            ch.prefetch(1);
            logger.debug(`waiting for messages from Rabbit MQ Server ${queue}`);
            ch.consume(queue, (msg) => {
                const data = JSON.parse(msg.content.toString());
                logger.debug('Received Message from Queue');
                sendRegistrationEmail(data)
                    .then(() => {
                        ch.ack(msg);
                    })
                    .catch((e) => {
                        logger.error(`Error when sending Email ${e}`);
                        ch.reject(msg);
                    });
            }, { noAck: false });
        });
    });
};


export default newUserConsumer;

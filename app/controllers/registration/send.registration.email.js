import Q from 'q';
import swig from 'swig';
import path from 'path';
import sendMail from '../../utils/mailer';
import config from '../../../config';


const sendRegistrationEmail = async(data) => {
    const defer = Q.defer();
    try {
        // const template = swig.compileFile(path.join(__dirname, '../../Email/templates/new.user.html'));
        // const message = template(data);
        const message = `Hello Akin you have registered, please use this link to activate account ${data.verification_code}`;
        const { sender } = config.mailer.registration;// Sender in config;
        const { subject } = config.mailer.registration; // Subject in Config;
        const user = data.email;
        const payload = {
            user,
            sender,
            subject,
            message
        };
        await sendMail(payload);
        defer.resolve('Email Sent');
    } catch (e) {
        defer.reject(e);
    }
    return defer.promise;
};


export default sendRegistrationEmail;

import Q from 'q';
import sgMail from '@sendgrid/mail';
import config from '../../config';

sgMail.setApiKey(config.SEND_GRID_API_KEY);


const sendMail = (options) => {
    const defer = Q.defer();
    const msg = {
        to: options.user,
        from: options.sender,
        subject: options.subject,
        text: options.message,
        html: options.message
    };
    sgMail.send(msg)
        .then((response) => {
            defer.resolve(response);
        })
        .catch((error) => {
            defer.reject(error);
        });
    return defer.promise;
};

export default sendMail;


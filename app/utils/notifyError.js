import axios from 'axios';
import moment from 'moment';
import config from '../../config';


const notifyOnError = (error) => {
    const environment = process.env.NODE_ENV || 'development';
    const errorObj = {
        error,
        dateTime: moment().format('YYYY-MM-DD'),
        service_name: `${config.serviceName} on ${environment}`,
        error_type: error.substring(7),
        error_code: error.lineNumber ? error.lineNumber : 'NIL',
        error_description: `Error log${JSON.stringify(error)}`
    };
    const options = {
        json: true,
        headers: {
            Accept: 'application/json'
        }
    };
    axios.post(config.errorNotificationWebHook, errorObj, options)
        .then(() => {
            logger.info('hello');
        })
        .catch((err) => {
            logger.error(err);
        });
};

export default notifyOnError;

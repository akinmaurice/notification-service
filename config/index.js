import path from 'path';
import util from 'util';

import development from './env/development';
import test from './env/test';
import production from './env/production';

const extend = (util)._extend;
const defaults = {
    root: path.normalize(`${__dirname}/..`),
    serviceName: 'Notification Service',
    errorNotificationWebHook: 'https://hooks.zapier.com/hooks/catch/4243471/0tp5s3/',
    messageQueue: {
        userRegistration: 'newUserRegistrationNotify'
    },
    mailer: {
        registration: {
            subject: 'Account Registration',
            sender: 'akin@gmail.com'
        }
    }
};

const environment = {
    development: extend(development, defaults),
    test: extend(test, defaults),
    production: extend(production, defaults)
}[process.env.NODE_ENV || 'development'];

export default environment;

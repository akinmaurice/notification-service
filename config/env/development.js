const development = {
    PORT: process.env.PORT,
    SEND_GRID_API_KEY: process.env.SEND_GRID_API_KEY,
    RABBIT_MQ_URL: process.env.RABBIT_MQ_DEV_URL
};

export default development;

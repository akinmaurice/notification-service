const test = {
    PORT: process.env.PORT,
    SEND_GRID_API_KEY: process.env.SEND_GRID_API_KEY,
    RABBIT_MQ_URL: process.env.RABBIT_MQ_TEST_URL
};

export default test;

FROM node:10.14.2-alpine


# Create app directory
RUN mkdir -p /usr/src/notification
WORKDIR /usr/src/notification

COPY package.json /usr/src/notification

RUN apk update && apk upgrade \
	&& apk add --no-cache git \
	&& apk --no-cache add --virtual builds-deps build-base python \
	&& npm install -g nodemon gulp node-gyp node-pre-gyp && npm install\
	&& npm rebuild bcrypt --build-from-source

# Bundle app source
COPY . /usr/src/notification

EXPOSE 3072


CMD ["npm", "start"]


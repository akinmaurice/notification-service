import chai from 'chai';
import sinon from 'sinon';
import sgMail from '@sendgrid/mail';

import sendMail from '../../app/utils/mailer';

const should = chai.should();
const { expect } = chai;
let sandbox;


describe('It tests Function to send email', () => {
    beforeEach(() => {
        sandbox = sinon.createSandbox();
    });

    afterEach(() => {
        sandbox.restore();
    });
    it('Should send email', async() => {
        const payload = {
            to: 'user@gmail.com',
            from: 'sender@gmail.com',
            subject: 'subject@gmail.com',
            text: 'message',
            html: 'message'
        };
        const result = {
            sent: true,
            receiver: 'user@gmail.com'
        };
        sandbox.stub(sgMail, 'send').returns(Promise.resolve(result));
        const response = await sendMail(payload);
        response.should.equal(result);
    });
});

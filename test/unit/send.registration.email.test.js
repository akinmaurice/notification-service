import chai from 'chai';
import sinon from 'sinon';
import sgMail from '@sendgrid/mail';
import sendRegistrationEmail from '../../app/controllers/registration/send.registration.email';
import sendMail from '../../app/utils/mailer';

const should = chai.should();
const { expect } = chai;
let sandbox;


describe('It tests Function to send welcome email', () => {
    beforeEach(() => {
        sandbox = sinon.createSandbox();
    });

    afterEach(() => {
        sandbox.restore();
    });
    it('Should send email to new user', async() => {
        const payload = {
            name: 'Akin',
            email: 'akin@ya.com'
        };
        const result = {
            sent: true,
            receiver: 'user@gmail.com'
        };
        sandbox.stub(sgMail, 'send').returns(Promise.resolve(result));
        const response = await sendRegistrationEmail(payload);
        response.should.equal('Email Sent');
    });
});
